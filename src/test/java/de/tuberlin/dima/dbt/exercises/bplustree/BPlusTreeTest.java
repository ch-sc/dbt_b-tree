package de.tuberlin.dima.dbt.exercises.bplustree;

import java.util.stream.IntStream;

import org.junit.Test;

import static de.tuberlin.dima.dbt.exercises.bplustree.BPlusTreeUtilities.keys;
import static de.tuberlin.dima.dbt.exercises.bplustree.BPlusTreeUtilities.newLeaf;
import static de.tuberlin.dima.dbt.exercises.bplustree.BPlusTreeUtilities.newNode;
import static de.tuberlin.dima.dbt.exercises.bplustree.BPlusTreeUtilities.newTree;
import static de.tuberlin.dima.dbt.exercises.bplustree.BPlusTreeUtilities.nodes;
import static de.tuberlin.dima.dbt.exercises.bplustree.BPlusTreeUtilities.values;
import static de.tuberlin.dima.dbt.grading.BPlusTreeMatcher.isTree;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class BPlusTreeTest {
	
	private BPlusTree tree;
	
	///// Lookup tests
	
	
	@Test
	public void findKeyInLeaf() {
		// given
		tree = newTree(newLeaf(keys(1, 2, 3), values("a", "b", "c")));
		// when
		String value = tree.lookup(2);
		// then
		assertThat(value, is("b"));
	}
	
	
	@Test
	public void findNoKeyInLeaf() {
		// given
		tree = newTree(newLeaf(keys(1, 3), values("a", "c")));
		// when
		String value = tree.lookup(2);
		// then
		assertThat(value, is(nullValue()));
	}
	
	
	@Test
	public void findKeyInChild() {
		// given
		tree = newTree(newNode(keys(3),
				nodes(newLeaf(keys(1, 2), values("a", "b")),
						newLeaf(keys(3, 4), values("c", "d")))));
		// when
		String value = tree.lookup(1);
		// then
		assertThat(value, is("a"));
	}
	
	
	@Test
	public void findNoKeyInChild() {
		// given
		tree = newTree(newNode(keys(3),
				nodes(newLeaf(keys(1, 3), values("a", "c")),
						newLeaf(keys(5, 7), values("e", "g")))));
		// when
		String value = tree.lookup(6);
		// then
		assertThat(value, is(nullValue()));
	}
	
	
	@Test
	public void findAllChildrenInFullTree() {
		// given
		tree = newTree(newNode(keys(3, 5, 7, 9),
				nodes(
						newLeaf(keys(-1, 0, 1, 2), values("y", "z", "a", "b")),
						newLeaf(keys(3, 4), values("c", "d")),
						newLeaf(keys(5, 6), values("e", "f")),
						newLeaf(keys(7, 8), values("g", "h")),
						newLeaf(keys(9, 10, 11, 12), values("i", "j", "k", "l"))
				)),
				4);
		// when
		String[] values = IntStream.range(-1, 14)
				.mapToObj(i -> tree.lookup(i))
				.toArray(String[]::new);
		
		// then
		int i = 0;
		assertThat(values[i++], is("y"));
		assertThat(values[i++], is("z"));
		assertThat(values[i++], is("a"));
		assertThat(values[i++], is("b"));
		assertThat(values[i++], is("c"));
		assertThat(values[i++], is("d"));
		assertThat(values[i++], is("e"));
		assertThat(values[i++], is("f"));
		assertThat(values[i++], is("g"));
		assertThat(values[i++], is("h"));
		assertThat(values[i++], is("i"));
		assertThat(values[i++], is("j"));
		assertThat(values[i++], is("k"));
		assertThat(values[i++], is("l"));
	}
	
	///// Insertion tests
	
	
	@Test
	public void insertIntoLeaf() {
		// given
		tree = newTree(newLeaf(keys(1, 3), values("a", "c")));
		// when
		tree.insert(2, "b");
		// then
		assertThat(tree, isTree(
				newTree(newLeaf(keys(1, 2, 3), values("a", "b", "c")))));
	}
	
	
	@Test
	public void splitLeafs() {
		// given
		tree = newTree(newNode(keys(3),
				nodes(newLeaf(keys(1, 2), values("a", "b")),
						newLeaf(keys(3, 4, 5, 6),
								values("c", "d", "e", "f")))));
		// when
		tree.insert(7, "g");
		// then
		assertThat(tree, isTree(newTree(newNode(
				keys(3, 5),
				nodes(newLeaf(keys(1, 2), values("a", "b")),
						newLeaf(keys(3, 4), values("c", "d")),
						newLeaf(keys(5, 6, 7), values("e", "f", "g")))))));
	}
	
	
	@Test
	public void splitNestedLeafs() {
		// given
		tree = newTree(newNode(keys(41),
				nodes(
						newNode(keys(12, 28), nodes(
								newLeaf(keys(1, 5, 9), values("a", "b", "c")),
								newLeaf(keys(12, 15, 17, 19), values("a", "b", "c", "d")),
								newLeaf(keys(28, 33, 37), values("e", "f", "g"))
						)),
						newNode(keys(46, 67), nodes(
								newLeaf(keys(41, 45), values("a", "b")),
								newLeaf(keys(46, 53, 59, 60), values("c", "d", "e", "f")),
								newLeaf(keys(67, 71, 83, 99), values("h", "i", "j", "k"))
						))
				)));
		
		// when
		tree.insert(61, "g");
		// then
		assertThat(tree, isTree(newTree(newNode(keys(41),
				nodes(
						newNode(keys(12, 28), nodes(
								newLeaf(keys(1, 5, 9), values("a", "b", "c")),
								newLeaf(keys(12, 15, 17, 19), values("a", "b", "c", "d")),
								newLeaf(keys(28, 33, 37), values("e", "f", "g"))
						)),
						newNode(keys(46, 59, 67), nodes(
								newLeaf(keys(41, 45), values("a", "b")),
								newLeaf(keys(46, 53), values("c", "d")),
								newLeaf(keys(59, 60, 61), values("e", "f", "g")),
								newLeaf(keys(67, 71, 83, 99), values("h", "i", "j", "k"))
						))
				)))));
	}
	
	
	@Test
	public void splitInnerNode() {
		// given
		tree = newTree(newNode(keys(41),
				nodes(
						newNode(keys(12, 28), nodes(
								newLeaf(keys(1, 5), values("a", "b"), 2),
								newLeaf(keys(12, 15), values("a", "b"), 2),
								newLeaf(keys(28, 33), values("e", "f"), 2)
						), 2),
						newNode(keys(46, 67), nodes(
								newLeaf(keys(41, 45), values("a", "b"), 2),
								newLeaf(keys(46, 53), values("c", "d"), 2),
								newLeaf(keys(67, 71), values("h", "i"), 2)
						), 2)
				), 2), 2);
		
		// when
		tree.insert(61, "g");
		
		// then
		assertThat(tree, isTree(newTree(newNode(keys(41, 53),
				nodes(
						newNode(keys(12, 28), nodes(
								newLeaf(keys(1, 5), values("a", "b"), 2),
								newLeaf(keys(12, 15), values("a", "b"), 2),
								newLeaf(keys(28, 33), values("e", "f"), 2)
						), 2),
						newNode(keys(46), nodes(
								newLeaf(keys(41, 45), values("a", "b"), 2),
								newLeaf(keys(46), values("c"), 2)
						), 2),
						newNode(keys(67), nodes(
								newLeaf(keys(53, 61), values("d", "g"), 2),
								newLeaf(keys(67, 71), values("h", "i"), 2)
						), 2)
				), 2), 2)));
	}
	
	
	@Test
	public void splitRoot() {
		// given
		tree = newTree(newNode(keys(3, 7, 11, 15),
				nodes(newLeaf(keys(1, 2), values("a", "b")),
						newLeaf(keys(3, 4, 5, 6), values("c", "d", "e", "f")),
						newLeaf(keys(7, 8, 9, 10), values("g", "h", "i", "j")),
						newLeaf(keys(11, 12, 13, 14), values("k", "l", "m", "n")),
						newLeaf(keys(15, 16, 17, 19), values("o", "p", "q", "s")))));
		// when
		tree.insert(18, "r");
		
		// then
		assertThat(tree, isTree(
				newTree(newNode(keys(11), nodes(
						newNode(keys(3, 7), nodes(
								newLeaf(keys(1, 2), values("a", "b")),
								newLeaf(keys(3, 4, 5, 6), values("c", "d", "e", "f")),
								newLeaf(keys(7, 8, 9, 10), values("g", "h", "i", "j"))
						)),
						newNode(keys(15, 17), nodes(
								newLeaf(keys(11, 12, 13, 14), values("k", "l", "m", "n")),
								newLeaf(keys(15, 16), values("o", "p")),
								newLeaf(keys(17, 18, 19), values("q", "r", "s"))
						))))
				)));
	}
	
	
	/**
	 * Todo: fix insert when root is full
	 * E.method: INSERT(key:73, value:QmP)
	 * tree:
	 * [82,87,102,106] => [RNj,EIL,bOc,idj]
	 * <p>
	 * I started with the tree:
	 * [82,87,102,106] => [RNj,EIL,bOc,idj]
	 * <p>
	 * I tried to insert the pair: 73 => QmP
	 * <p>
	 * I expected the resulting tree to be:
	 * [87,,,] =>
	 * [73,82,,] => [QmP,RNj,,]
	 * [87,102,106,] => [EIL,bOc,idj,]
	 * <p>
	 * However, the actual tree was:
	 * [73,82,87,102] => [QmP,RNj,EIL,bOc]
	 */
	@Test
	public void insertToFullRoot() {
		
		// Arrange
		tree = newTree(newLeaf(keys(82, 87, 102, 106), values("RNj", "EIL", "bOc", "idj"), 4));
		
		// Act
		tree.insert(74, "QmP");
		
		// Assert
		
		assertThat(tree, isTree(newTree(
				newNode(keys(87),
						nodes(
								newLeaf(keys(74, 82), values("QmP", "RNj"), 4),
								newLeaf(keys(87, 102, 106), values("EIL", "bOc", "idj"), 4)
						), 4))
		));
		
	}
	
	///// Deletion tests
	
	
	@Test
	public void deleteFromLeaf() {
		// given
		tree = newTree(newLeaf(keys(1, 2, 3), values("a", "b", "c")));
		// when
		String value = tree.delete(2);
		// then
		assertThat(value, is("b"));
		assertThat(tree, isTree(
				newTree(newLeaf(keys(1, 3), values("a", "c")))));
	}
	
	
	@Test
	public void deleteFromChild() {
		// given
		tree = newTree(newNode(
				keys(4), nodes(newLeaf(keys(1, 2, 3), values("a", "b", "c")), newLeaf(keys(4, 5), values("d", "e")))));
		// when
		String value = tree.delete(1);
		// then
		assertThat(value, is("a"));
		assertThat(tree, isTree(newTree(newNode(
				keys(4), nodes(newLeaf(keys(2, 3), values("b", "c")),
						newLeaf(keys(4, 5), values("d", "e")))))));
	}
	
	
	@Test
	public void deleteFromChildStealFromSibling() {
		// given
		tree = newTree(newNode(
				keys(3), nodes(newLeaf(keys(1, 2), values("a", "b")),
						newLeaf(keys(3, 4, 5), values("c", "d", "e")))));
		// when
		String value = tree.delete(1);
		// then
		assertThat(value, is("a"));
		assertThat(tree, isTree(newTree(newNode(
				keys(4), nodes(newLeaf(keys(2, 3), values("b", "c")),
						newLeaf(keys(4, 5), values("d", "e")))))));
		
	}
	
	
	@Test
	public void deleteFromChildMergeWithSibling() {
		// given
		tree = newTree(newNode(keys(3, 5),
				nodes(newLeaf(keys(1, 2), values("a", "b")),
						newLeaf(keys(3, 4), values("c", "d")),
						newLeaf(keys(5, 6), values("e", "f")))));
		// when
		String value = tree.delete(2);
		// then
		assertThat(value, is("b"));
		assertThat(tree, isTree(newTree(newNode(
				keys(5), nodes(newLeaf(keys(1, 3, 4), values("a", "c", "d")),
						newLeaf(keys(5, 6), values("e", "f")))))));
	}
	
	/*
	 * .method: DELETE(key:31)
	 * tree:
	 * [36,64,,] =>
	 *   [16,31,,] => [kYz,oxn,,]
	 *   [36,47,,] => [QuO,mUi,,]
	 *   [64,68,,] => [fNF,AtK,,]
	 *
	 * I started with the tree:
	 * [36,64,,] =>
	 *   [16,31,,] => [kYz,oxn,,]
	 *   [36,47,,] => [QuO,mUi,,]
	 *   [64,68,,] => [fNF,AtK,,]
	 *
	 * I tried to delete the key: 31
	 *
	 * I expected the resulting tree to be:
	 * [64,,,] =>
	 *   [16,36,47,] => [kYz,QuO,mUi,]
	 *   [64,68,,] => [fNF,AtK,,]
	 *
	 * However, the actual tree was:
	 * [47,,,] =>
	 *   [16,36,,] => [kYz,QuO,,]
	 *   [47,64,68,] => [mUi,fNF,AtK,]
	 *
	 * */
	
	
	/**
	 * I started with the tree:
	 * [60,,,] =>
	 * [42,51,,] => [UCC,RVS,,]
	 * [60,84,,] => [zmB,oOI,,]
	 * I tried to delete the key: 72
	 * I expected the resulting tree to be:
	 * [60,,,] =>
	 * [42,51,,] => [UCC,RVS,,]
	 * [60,84,,] => [zmB,oOI,,]
	 * However, the actual tree was:
	 * [51,,,] =>
	 * [42,51,,] => [UCC,RVS,,]
	 * [84,,,] => [oOI,,,]
	 */
	@Test
	public void deleteNonExistentChild() {
		
		// Arrange
		final int cap = 4;
		tree = newTree(newNode(keys(60), nodes(
				newLeaf(keys(42, 51), values("UCC", "RVS"), cap),
				newLeaf(keys(60, 85), values("zmB", "oOI"), cap)
		), cap));
		
		// Act
		String value = tree.delete(72);
		
		
		// Assert
		assertThat(value, is(nullValue()));
		assertThat(tree, isTree(newTree(newNode(keys(60), nodes(
				newLeaf(keys(42, 51), values("UCC", "RVS"), cap),
				newLeaf(keys(60, 85), values("zmB", "oOI"), cap)
		), cap))));
	}
	
	
	@Test
	public void deleteChild() {
		
		// Arrange
		final int CAPACITY = 4;
		tree = newTree(newNode(keys(41), nodes(
				newNode(
						keys(12, 28),
						nodes(
								newLeaf(keys(1, 5, 9), values("a", "b", "c")),
								newLeaf(keys(12, 15, 19), values("d", "e", "f"), CAPACITY),
								newLeaf(keys(28, 33, 37), values("g", "h", "i"), CAPACITY)),
						CAPACITY),
				newNode(keys(46, 76),
						nodes(
								newLeaf(keys(41, 45), values("j", "k")),
								newLeaf(keys(46, 53, 59), values("m", "n", "o"), CAPACITY),
								newLeaf(keys(67, 71, 83, 99), values("p", "q", "r", "s"), CAPACITY)),
						CAPACITY)
				)),
				CAPACITY);
		
		// Act
		String value = tree.delete(12);
		
		
		// Assert
		assertThat(value, is("d"));
		assertThat(tree, isTree(newTree(newNode(keys(41), nodes(
				newNode(
						keys(12, 28),
						nodes(
								newLeaf(keys(1, 5, 9), values("a", "b", "c")),
								newLeaf(keys(15, 19), values("e", "f"), CAPACITY),
								newLeaf(keys(28, 33, 37), values("g", "h", "i"), CAPACITY)),
						CAPACITY),
				newNode(keys(46, 76),
						nodes(
								newLeaf(keys(41, 45), values("j", "k")),
								newLeaf(keys(46, 53, 59), values("m", "n", "o"), CAPACITY),
								newLeaf(keys(67, 71, 83, 99), values("p", "q", "r", "s"), CAPACITY)),
						CAPACITY)
				)),
				CAPACITY)));
		
	}
	
	
	@Test
	public void deleteChildAndReorderTree_I() {
		
		// Arrange
		final int CAPACITY = 4;
		tree = newTree(newNode(keys(41), nodes(
				newNode(
						keys(12, 28),
						nodes(
								newLeaf(keys(1, 5, 9), values("a", "b", "c")),
								newLeaf(keys(15, 19), values("e", "f"), CAPACITY),
								newLeaf(keys(28, 33), values("g", "h"), CAPACITY)),
						CAPACITY),
				newNode(keys(46, 76),
						nodes(
								newLeaf(keys(41, 45), values("j", "k")),
								newLeaf(keys(46, 53, 59), values("m", "n", "o"), CAPACITY),
								newLeaf(keys(67, 71, 83, 99), values("p", "q", "r", "s"), CAPACITY)),
						CAPACITY)
				)),
				CAPACITY);
		
		// Act
		String value = tree.delete(15);
		
		
		// Assert
		assertThat(value, is("e"));
		assertThat(tree, isTree(newTree(newNode(keys(41), nodes(
				newNode(
						keys(9, 28),
						nodes(
								newLeaf(keys(1, 5), values("a", "b")),
								newLeaf(keys(9, 19), values("c", "f"), CAPACITY),
								newLeaf(keys(28, 33), values("g", "h"), CAPACITY)),
						CAPACITY),
				newNode(keys(46, 76),
						nodes(
								newLeaf(keys(41, 45), values("j", "k")),
								newLeaf(keys(46, 53, 59), values("m", "n", "o"), CAPACITY),
								newLeaf(keys(67, 71, 83, 99), values("p", "q", "r", "s"), CAPACITY)),
						CAPACITY)
				)),
				CAPACITY)));
	}
	
	
	/**
	 * method invocation: DELETE(key:54)
	 * tree:
	 * [66,,,] =>
	 * [47,54,,] => [UEN,unV,,]
	 * [66,76,,] => [yQc,ZxE,,]
	 * result: unV
	 * tree:
	 * [,,,] =>
	 * [47,66,76,] => [UEN,yQc,ZxE,]
	 * I started with the tree:
	 * [66,,,] =>
	 * [47,54,,] => [UEN,unV,,]
	 * [66,76,,] => [yQc,ZxE,,]
	 * I tried to delete the key: 54
	 * I expected the resulting tree to be:
	 * [47,66,76,] => [UEN,yQc,ZxE,]
	 * However, the actual tree was:
	 * [,,,] =>
	 * [47,66,76,] => [UEN,yQc,ZxE,]
	 */
	@Test
	public void mergeRootAfterDelete() {
		
		// Arrange
		tree = newTree(newNode(keys(66),
				nodes(newLeaf(keys(47, 54), values("a", "b")),
						newLeaf(keys(66, 76), values("c", "d"))
				)));
		
		// Act
		String value = tree.delete(54);
		
		// Assert
		assertThat(value, is("b"));
		assertThat(tree, isTree(newTree(newLeaf(keys(47,66,76), values("a", "c", "d")))));
		
	}
	
	
	/**
	 * method invocation: DELETE(key:113)
	 * tree:
	 * [113,138,,] =>
	 *   [92,106,,] => [bzE,fVZ,,]
	 *   [113,124,,] => [Lng,FpN,,]
	 *   [138,148,,] => [hsa,FRT,,]
	 * result: Lng
	 * tree:
	 * [138,,,] =>
	 *   [92,106,124,] => [bzE,fVZ,FpN,]
	 *   [138,148,,] => [hsa,FRT,,]
	 * I started with the tree:
	 * [113,138,,] =>
	 *   [92,106,,] => [bzE,fVZ,,]
	 *   [113,124,,] => [Lng,FpN,,]
	 *   [138,148,,] => [hsa,FRT,,]
	 * I tried to delete the key: 113
	 *
	 * I expected the resulting tree to be:
	 * [113,,,] =>
	 *   [92,106,,] => [bzE,fVZ,,]
	 *   [124,138,148,] => [FpN,hsa,FRT,]
	 *
	 * However, the actual tree was:
	 * [138,,,] =>
	 *   [92,106,124,] => [bzE,fVZ,FpN,]
	 *   [138,148,,] => [hsa,FRT,,]
	 */
	@Test
	public void mergeRootAfterDelete_II(){
		
		// Arrange
		tree = newTree(newNode(keys(113,138),
				nodes(newLeaf(keys(92,106), values("a", "b")),
						newLeaf(keys(113,124), values("c", "d")),
						newLeaf(keys(138,148), values("hsa", "FRT"))
				)));
		
		// Act
		String value = tree.delete(113);
		
		// Assert
		assertThat(value, is("c"));
		assertThat(tree, isTree(newTree(newNode(keys(113),
				nodes(newLeaf(keys(92,106), values("a", "b")),
						newLeaf(keys(124,138,148), values("d", "hsa", "FRT"))
				)))));
	}
	
	@Test
	public void mergeRootAfterDelete_III(){
	    
	    // Arrange
		/*[112,,,] =>
			  [77,95,,] =>
				[51,60,,] => [KQr,rUj,,]
				[77,81,,] => [pDg,OEE,,]
				[95,100,,] => [psU,aev,,]
			  [138,158,,] =>
				[112,127,,] => [eZo,whD,,]
				[138,143,,] => [mHX,hZM,,]
				[158,160,,] => [Lme,PTN,,]
		*/
		
		tree = newTree(newNode(keys(112), nodes(
				newNode(keys(77,95), nodes(
						newLeaf(keys(51,60), values("a", "b")),
						newLeaf(keys(77,81), values("c", "d")),
						newLeaf(keys(95,100), values("hsa", "FRT")))),
				newNode(keys(138, 158), nodes(
						newLeaf(keys(112,127), values("a", "b")),
						newLeaf(keys(138,143), values("c", "d")),
						newLeaf(keys(158,160), values("hsa", "FRT"))
				))))
		);
		
		// Act
		String value = tree.delete(160);
		
		// Assert
		/*[77,95,112,138] =>
			  [51,60,,] => [KQr,rUj,,]
			  [77,81,,] => [pDg,OEE,,]
			  [95,100,,] => [psU,aev,,]
			  [112,127,,] => [eZo,whD,,]
			  [138,143,158,] => [mHX,hZM,Lme,]
		 */
		assertThat(value, is("FRT"));
		assertThat(tree, isTree(newTree(newNode(keys(77,95, 112, 138), nodes(
						newLeaf(keys(51,60), values("a", "b")),
						newLeaf(keys(77,81), values("c", "d")),
						newLeaf(keys(95,100), values("hsa", "FRT")),
						newLeaf(keys(112,127), values("a", "b")),
						newLeaf(keys(138,143, 158), values("c", "d", "hsa"))
				)))));
	}
	
	@Test
	public void mergeInnerNodesAfterDelete_I(){
	    
	    // Arrange
	    /*
	    * [144,,,] =>
			  [76,103,120,] =>
				[59,69,,] => [eva,VoA,,]
				[76,90,,] => [amK,HTy,,]
				[103,109,,] => [jKh,dgJ,,]
				[120,130,,] => [cZG,hJK,,]
			  [161,180,,] =>
				[144,150,,] => [dex,lTq,,]
				[161,168,,] => [Dcp,FFi,,]
				[180,186,,] => [Jti,HbI,,]
	    * */
	    
		tree = newTree(newNode(keys(144), nodes(
				newNode(keys(76,103,120), nodes(
						newLeaf(keys(59,69), values("a", "b")),
						newLeaf(keys(76,90), values("c", "d")),
						newLeaf(keys(103,109), values("c", "d")),
						newLeaf(keys(120,130), values("hsa", "FRT")))),
				newNode(keys(161,180), nodes(
						newLeaf(keys(144,150), values("a", "b")),
						newLeaf(keys(161,168), values("c", "d")),
						newLeaf(keys(180,186), values("hsa", "FRT"))
				))))
		);
		
		// Act
		String value = tree.delete(168);
		
		// Assert
		/*[120,,,] =>
			  [76,103,,] =>
				[59,69,,] => [eva,VoA,,]
				[76,90,,] => [amK,HTy,,]
				[103,109,,] => [jKh,dgJ,,]
			  [144,161,,] =>
				[120,130,,] => [cZG,hJK,,]
				[144,150,,] => [dex,lTq,,]
				[161,180,186,] => [Dcp,Jti,HbI,]
		 */
		assertThat(value, is("d"));
		assertThat(tree, isTree(newTree(newNode(keys(120), nodes(
				newNode(keys(76,103), nodes(
						newLeaf(keys(59,69), values("a", "b")),
						newLeaf(keys(76,90), values("c", "d")),
						newLeaf(keys(103,109), values("c", "d")))),
				newNode(keys(144,161), nodes(
						newLeaf(keys(120,130), values("hsa", "FRT")),
						newLeaf(keys(144,150), values("a", "b")),
						newLeaf(keys(161,180,186), values("c", "hsa", "FRT"))
				))))
		)));
	}
	
	
	
	@Test
	public void mergeInnerNodesAfterDelete_II(){
		
		// Arrange
	    /*
	    * [165,,,] =>
			  [125,149,,] =>
				[106,123,,] => [AYL,oAr,,]
				[125,141,,] => [ylZ,jYc,,]
				[149,155,,] => [rKj,HKp,,]
			  [189,207,225,] =>
				[165,182,,] => [pUF,wFO,,]
				[189,199,,] => [zYI,MOM,,]
				[207,222,,] => [Eom,fce,,]
				[225,239,,] => [hel,tBq,,]
	    * */
		
		tree = newTree(newNode(keys(165), nodes(
				newNode(keys(125,149), nodes(
						newLeaf(keys(106,123), values("c", "d")),
						newLeaf(keys(125,141), values("c", "d")),
						newLeaf(keys(149,155), values("hsa", "FRT")))),
				newNode(keys(189,207,225), nodes(
						newLeaf(keys(165,182), values("a", "b")),
						newLeaf(keys(189,199), values("c", "d")),
						newLeaf(keys(207,222), values("c", "d")),
						newLeaf(keys(225,239), values("hsa", "FRT"))
				))))
		);
		
		// Act
		String value = tree.delete(141);
		
		// Assert
		/*[189,,,] =>
			  [125,165,,] =>
				[106,123,,] => [AYL,oAr,,]
				[125,149,155,] => [ylZ,rKj,HKp,]
				[165,182,,] => [pUF,wFO,,]
			  [207,225,,] =>
				[189,199,,] => [zYI,MOM,,]
				[207,222,,] => [Eom,fce,,]
				[225,239,,] => [hel,tBq,,]
		 */
		assertThat(value, is("d"));
		assertThat(tree, isTree(newTree(newNode(keys(189), nodes(
				newNode(keys(125,165), nodes(
						newLeaf(keys(106,123), values("c", "d")),
						newLeaf(keys(125,149,155), values("c", "hsa", "FRT")),
						newLeaf(keys(165,182), values("a", "b"))
				)),
				newNode(keys(207,225), nodes(
						newLeaf(keys(189,199), values("c", "d")),
						newLeaf(keys(207,222), values("c", "d")),
						newLeaf(keys(225,239), values("hsa", "FRT"))
				))))
		)));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
