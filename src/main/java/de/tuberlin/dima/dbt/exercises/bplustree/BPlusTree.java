package de.tuberlin.dima.dbt.exercises.bplustree;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.ArrayUtils;

import static de.tuberlin.dima.dbt.exercises.bplustree.BPlusTreeUtilities.keys;
import static de.tuberlin.dima.dbt.exercises.bplustree.BPlusTreeUtilities.nodes;
import static de.tuberlin.dima.dbt.exercises.bplustree.BPlusTreeUtilities.values;

/**
 * Implementation of a B+ tree.
 * <p>
 * The capacity of the tree is given by the capacity argument to the
 * constructor. Each node has at least {capacity/2} and at most {capacity} many
 * keys. The values are strings and are stored at the leaves of the tree.
 * <p>
 * For each inner node, the following conditions hold:
 * <p>
 * {pre}
 * Integer[] keys = innerNode.getKeys();
 * Node[] children = innerNode.getChildren();
 * {pre}
 * <p>
 * - All keys in {children[i].getKeys()} are smaller than {keys[i]}.
 * - All keys in {children[j].getKeys()} are greater or equal than {keys[i]}
 * if j > i.
 */
public class BPlusTree {
	
	///// Leave these methods unchanged
	
	private int capacity = 0;
	
	private Node root;
	
	
	public BPlusTree(int capacity) {
		this(new LeafNode(capacity), capacity);
	}
	
	
	public BPlusTree(Node root, int capacity) {
		assert capacity % 2 == 0;
		this.capacity = capacity;
		this.root = root;
	}
	
	
	public Node rootNode() {
		return root;
	}
	
	
	public String toString() {
		return new BPlusTreePrinter(this).toString();
	}
	
	
	private LeafNode findLeafNode(Integer key, Node node) {
		return findLeafNode(key, node, null);
	}
	
	
	///// Public API
	///// These can be left unchanged
	
	
	/**
	 * Lookup the value stored under the given key.
	 *
	 * @return The stored value, or {null} if the key does not exist.
	 */
	public String lookup(Integer key) {
		System.out.println("---------------------------------------------------------------------");
		System.out.println("method invocation: LOOKUP(key:" + key + ")\ntree:\n" + this.toString());
		LeafNode leafNode = findLeafNode(key, root);
		String result = lookupInLeafNode(key, leafNode);
		System.out.println("result: " + result + "\ntree:\n" + this.toString());
		return result;
	}
	
	
	/**
	 * Insert the key/value pair into the B+ tree.
	 */
	public void insert(int key, String value) {
		System.out.println("---------------------------------------------------------------------");
		System.out.println("method invocation: INSERT(key:" + key + ", value:" + value + ")\n"
				+ "tree:\n" + this.toString());
		Deque<InnerNode> parents = new LinkedList<>();
		LeafNode leafNode = findLeafNode(key, root, parents);
		insertIntoLeafNode(key, value, leafNode, parents);
		System.out.println("result:\ntree:\n" + this.toString());
	}
	
	
	/**
	 * Delete the key/value pair from the B+ tree.
	 *
	 * @return The original value, or {null} if the key does not exist.
	 */
	public String delete(Integer key) {
		System.out.println("---------------------------------------------------------------------");
		System.out.println("method invocation: DELETE(key:" + key + ")\ntree:\n" + this.toString());
		Deque<InnerNode> parents = new LinkedList<>();
		LeafNode leafNode = findLeafNode(key, root, parents);
		String result = deleteFromLeafNode(key, leafNode, parents);
		System.out.println("result: " + result + "\ntree:\n" + this.toString());
		return result;
	}
	
	
	///// Implement these methods
	
	
	private LeafNode findLeafNode(Integer key, Node node, Deque<InnerNode> parents) {
		if (node instanceof LeafNode) {
			return (LeafNode) node;
		} else {
			InnerNode innerNode = (InnerNode) node;
			if (parents != null) {
				parents.push(innerNode);
			}
			
			// TODO: traverse inner nodes to find leaf node
			Integer[] keys = innerNode.getKeys();
			for (int i = 0; i < keys.length; i++) {
				Integer tmpKey = keys[i];
				if (tmpKey == null || key < tmpKey) {
					return findLeafNode(key, innerNode.getChildren()[i], parents);
				}
				// node is full and search key is higher than right-most key -> return right-most (last) child
				else if (i == keys.length - 1) {
					return findLeafNode(key, innerNode.getChildren()[i + 1], parents);
				}
			}
			
			throw new IllegalArgumentException(String.format("Key not found in node: (%s) -> %s",
					key, Arrays.toString(keys)));
		}
	}
	
	
	private String lookupInLeafNode(Integer key, LeafNode node) {
		// TODO: lookup value in leaf node
		Integer[] keys = node.getKeys();
		for (int i = 0; i < keys.length; i++) {
			if (key.equals(keys[i])) {
				return node.getValues()[i];
			}
		}
		
		return null;
	}
	
	
	private void insertIntoLeafNode(Integer key, String value, LeafNode node, Deque<InnerNode> parents) {
		if (node == null) {
			// todo create new leaf and add it to the parent
			throw new IllegalStateException("leaf node is null...");
		}
		
		Integer[] keys = node.getKeys();
		String[] values = node.getValues();
		
		// node is already full
		if (countKeys(keys) == capacity) {
			splitAndInsert(key, value, node, parents);
			return;
		}
		
		for (int i = 0; i < keys.length; i++) {
			Integer k = keys[i];
			
			// if key is null, it's the end of the keys and there is still space to do the insert
			if (k == null) {
				keys[i] = key;
				values[i] = value;
				break;
			}
			// leaf is full -> split node!
			else if (i == capacity - 1) {
				splitAndInsert(key, value, node, parents);
				break;
			}
			// if key is smaller than current lookup key, insert key and shift the remaining keys and values
			else if (key < k) {
				insertAt(keys, key, i);
				insertAt(values, value, i);
				break;
			} else if (key.equals(k)) {
				throw new IllegalArgumentException(String.format("Key [%s] has already been inserted to tree.", key));
			}
		}
	}
	
	
	private static <T> void insertAt(T[] array, T value, int index) {
		for (int i = array.length - 1; i >= index; i--) {
			array[i] = i == index ? value : array[i - 1];
		}
	}
	
	
	private void insertIntoInnerNode(Integer key, Node leftLeaf, Node rightLeaf, Deque<InnerNode> parents) {
		
		// create a new root
		if (parents.peek() == null) {
			root = BPlusTreeUtilities.newNode(
					createArray(new Integer[] { key }, capacity),
					createArray(new Node[] { leftLeaf, rightLeaf }, capacity + 1),
					capacity);
			return;
		}
		
		InnerNode parent = parents.pop();
		Integer[] keys = parent.getKeys();
		
		if (countKeys(keys) == capacity) {
			// todo split
			splitAndInsert(key, leftLeaf, rightLeaf, parent, parents);
		} else {
			int keyIndex = getKeyIndex(keys, key);
			insertAt(keys, key, keyIndex);
			insertAt(parent.getChildren(), leftLeaf, keyIndex);
			parent.getChildren()[keyIndex + 1] = rightLeaf;
		}
	}
	
	
	private static int countKeys(Integer[] keys) {
		int keysCount = 0;
		for (Integer k : keys) {
			if (k == null) {
				break;
			}
			keysCount++;
		}
		return keysCount;
	}
	
	
	private void splitAndInsert(Integer key, String value, LeafNode node, Deque<InnerNode> parents) {
		
		if (node.getPayload().length != capacity) {
			throw new IllegalArgumentException(
					"Non-full nodes must not be split: " + Arrays.toString(node.getPayload()));
		}
		
		Integer[] keys = node.getKeys();
		
		Integer[] keyBuffer = ArrayUtils.add(keys, null);
		String[] valueBuffer = ArrayUtils.add(node.getValues(), null);
		
		int keyIndex = getKeyIndex(keys, key);
		
		insertAt(keyBuffer, key, keyIndex);
		insertAt(valueBuffer, value, keyIndex);
		
		int median = keys.length / 2;
		int medianKey = keyBuffer[median];
		
		LeafNode leftLeafNode = BPlusTreeUtilities.newLeaf(
				keys(createArray(Arrays.copyOfRange(keyBuffer, 0, median), capacity)),
				values(createArray(Arrays.copyOfRange(valueBuffer, 0, median), capacity)),
				capacity);
		LeafNode rightLeafNode = BPlusTreeUtilities.newLeaf(
				keys(createArray(Arrays.copyOfRange(keyBuffer, median, capacity + 1), capacity)),
				values(createArray(Arrays.copyOfRange(valueBuffer, median, capacity + 1), capacity)),
				capacity);
		
		insertIntoInnerNode(medianKey, leftLeafNode, rightLeafNode, parents);
	}
	
	
	private void splitAndInsert(Integer key, Node leftNode, Node rightNode, InnerNode fullNode,
			Deque<InnerNode> parents) {
		if (fullNode.getKeys().length != capacity) {
			throw new IllegalArgumentException(
					"Non-full node must not be split: " + Arrays.toString(fullNode.getPayload()));
		}
		
		Integer[] keys = fullNode.getKeys();
		
		Integer[] keyBuffer = ArrayUtils.add(keys, null);
		Node[] childrenBuffer = ArrayUtils.add(fullNode.getChildren(), null);
		
		int keyIndex = getKeyIndex(keys, key);
		
		keyBuffer = insert(keyIndex, keyBuffer, key);
		childrenBuffer[keyIndex] = rightNode;
		insertAt(childrenBuffer, leftNode, keyIndex);
		
		int median = keys.length / 2;
		int medianKey = keyBuffer[median];
		
		InnerNode leftInnerNode = BPlusTreeUtilities.newNode(
				keys(createArray(Arrays.copyOfRange(keyBuffer, 0, median), capacity)),
				nodes(createArray(Arrays.copyOfRange(childrenBuffer, 0, median + 1), capacity + 1)),
				capacity);
		InnerNode rightInnerNode = BPlusTreeUtilities.newNode(
				keys(createArray(Arrays.copyOfRange(keyBuffer, median + 1, capacity + 1), capacity)),
				nodes(createArray(Arrays.copyOfRange(childrenBuffer, median + 1, capacity + 2), capacity + 1)),
				capacity);
		
		insertIntoInnerNode(medianKey, leftInnerNode, rightInnerNode, parents);
	}
	
	
	@SuppressWarnings("unchecked")
	private static <T> T[] createArray(T[] source, int size) {
		T[] destination = (T[]) Array.newInstance(source.getClass().getComponentType(), size);
		if (size >= 0) {
			System.arraycopy(source, 0, destination, 0, source.length);
		}
		return destination;
	}
	
	
	private static int getKeyIndex(Integer[] keys, Integer key) {
		int keyIndex = 0;
		for (Integer k : keys) {
			if (k == null || key <= k) {
				break;
			} else {
				keyIndex++;
			}
		}
		return keyIndex;
	}
	
	
	private String deleteFromLeafNode(Integer key, LeafNode node, Deque<InnerNode> parents) {
		// TODO: delete value from leaf node (and propagate changes up)
		
		Integer[] keys = node.getKeys();
		String[] values = node.getValues();
		String deletedValue = null;
		boolean gotDeleted = false;
		for (int i = 0; i < keys.length; i++) {
			Integer k = keys[i];
			if (k == null || k > key) {
				if (!gotDeleted) {
					// delete key was not found
					return null;
				}
				// shift remaining entries to the left if possible
				keys[i - 1] = keys[i];
				values[i - 1] = values[i];
			} else if (k.equals(key)) {
				keys[i] = null;
				deletedValue = values[i];
				values[i] = null;
				gotDeleted = true;
			}
		}
		
		/*
		 * If a node does not have enough keys after deletion, it should first steal from its left sibling.
		 * If that is not possible, it should steal from its right sibling.
		 * If that is not possible, it should merge with its right sibling.
		 * Finally, if that is not possible it should merge with its left sibling.
		 */
		
		if (!isValid(node)) {
			stealOrMerge(key, parents);
		}
		
		return deletedValue;
	}

	private void stealOrMerge(Integer key, Deque<InnerNode> parents) {
		if (parents.peekFirst() == null) {
			return;
		}
		
		InnerNode parent = parents.pop();
		
		boolean success = stealFromSiblings(key, parent);
		
		if (!success) {
			parents.push(parent);
			if (parent.getChildren()[0] instanceof LeafNode)
				merge(key, parents);
			else
				mergeInnerNodes(key, parents);
		}
		
	}
	
	private void merge(Integer key, Deque<InnerNode> parents) {
		
		InnerNode parent = parents.pop();
		int parentChildIndex = getParentChildIndex(key, parent);
		
		Integer[] keys = parent.getKeys();
		int keysCount = countKeys(keys);
		Node[] children = parent.getChildren();
		Node node = children[parentChildIndex];
		
		Integer[] keyBuffer = ArrayUtils.EMPTY_INTEGER_OBJECT_ARRAY;
		String[] valueBuffer = ArrayUtils.EMPTY_STRING_ARRAY;
		
		// check if merge with right node is possible
		if (parentChildIndex < keysCount && parentChildIndex != -1) {
		
			keyBuffer = addArrayNonNullValues(keyBuffer, node.getKeys(), null, Integer.class);
			valueBuffer = addArrayNonNullValues(valueBuffer, (String[]) node.getPayload(), null, String.class);
		
			Node right = children[parentChildIndex + 1];
			keyBuffer = addArrayNonNullValues(keyBuffer, right.getKeys(), null, Integer.class);
			valueBuffer = addArrayNonNullValues(valueBuffer, (String[]) right.getPayload(), null, String.class);
			
			LeafNode leafNode = BPlusTreeUtilities.newLeaf(
					keys(ArrayUtils.subarray(keyBuffer, 0, keyBuffer.length)),
					values(ArrayUtils.subarray(valueBuffer, 0, keyBuffer.length)),
					capacity);

			children[parentChildIndex] = leafNode;
			int i = parentChildIndex + 1;
			children[i] = null;
			while (i < capacity && children[i + 1] != null) {
				children[i] = children[i + 1];
				children[i + 1] = null;
				i++;
			}
			i = parentChildIndex;
			keys[i] = null;
			while (i < capacity - 1 && keys[i + 1] != null) {
				keys[i] = keys[i + 1];
				keys[i + 1] = null;
				i++;
			}
			
		}
		
		// if not possible merge with left node
		else {
			Node left = children[parentChildIndex -1];
			keyBuffer = addArrayNonNullValues(keyBuffer, left.getKeys(), null, Integer.class);
			valueBuffer = addArrayNonNullValues(valueBuffer, (String[]) left.getPayload(), null, String.class);
			
			keyBuffer = addArrayNonNullValues(keyBuffer, node.getKeys(), null, Integer.class);
			valueBuffer = addArrayNonNullValues(valueBuffer, (String[]) node.getPayload(), null, String.class);
			
			
			LeafNode leafNode = BPlusTreeUtilities.newLeaf(
					keys(ArrayUtils.subarray(keyBuffer, 0, keyBuffer.length)),
					values(ArrayUtils.subarray(valueBuffer, 0, keyBuffer.length)),
					capacity);
					
			children[parentChildIndex -1 ] = leafNode;
			int i = parentChildIndex;
			children[i] = null;
			keys[i -1] = null;
			while (i < capacity && children[i + 1] != null) {
				children[i] = children[i + 1];
				i++;
			}
			i = parentChildIndex;
			keys[i] = null;
			while (i < capacity - 1 && keys[i + 1] != null) {
				keys[i] = keys[i + 1];
				i++;
			}
		}
		
		
		if (!isValid(parent)) {
			
			// this must work! Period.
			// if parent is invalid, it only has a single key and two children. All keys and all values are already
			// contained in both of the buffers. Thus, creating a new leaf from those buffers and setting it as the root
			// suffices.
			if (parent.equals(rootNode())) {
				root = BPlusTreeUtilities.newLeaf(
						createArray(keyBuffer, capacity),
						createArray(valueBuffer, capacity),
						capacity);
				return;
			}
			
			stealOrMerge(key, parents);
		}
	}
	
	
	private int getParentChildIndex(Integer key, InnerNode parent) {
		int nodeKeyIndex = -1;
		Integer[] parentKeys = parent.getKeys();
		for (int i = 0; i < parentKeys.length; i++) {
			
			Integer parentKey = parentKeys[i];
			if (parentKey == null) {
				break;
			}
			
			if (key >= parentKey) {
				nodeKeyIndex = i + 1;
				
			} else {
				nodeKeyIndex = i;
				break;
			}
		}
		
		return nodeKeyIndex;
	}
	
	
	private boolean stealFromSiblings(Integer key, InnerNode parent) {
		
		int nodeKeyIndex = getParentChildIndex(key, parent);
		Integer[] parentKeys = parent.getKeys();
		
		boolean success;
		
		if (nodeKeyIndex == -1) {
			// node index is referring to the last (right-most) node. Therefore, going from right to left only
			nodeKeyIndex = countKeys(parentKeys);
			success = stealFromLeft(parent, nodeKeyIndex);
		} else {
			// stealing from left nodes first
			success = stealFromLeft(parent, nodeKeyIndex);
			if (!success) {
				// could not even out the nodes to the right. The last (right-most) node is not valid at this point.
				// Hence, it needs to steal from its left neighbors -> going from right to left
				success = stealFromRight(parent, nodeKeyIndex);
			}
		}
		
		return success;
	}
	
	
	private boolean stealFromRight(InnerNode parent, int nodeKeyIndex) {
		Node[] children = parent.getChildren();
		Node left = children[nodeKeyIndex];
		Node right = children[nodeKeyIndex + 1];
		
		if (right == null) {
			return false;
		}
		
		// check whether left node is eligible for stealing from it
		if (countKeys(right.getKeys()) <= capacity / 2)
			return false;
		
		if (left instanceof LeafNode) {
			rightLeafStealing((LeafNode) left, (LeafNode) right, parent, nodeKeyIndex);
		} else {
			rightInnerNodeStealing((InnerNode) left, (InnerNode) right, parent, nodeKeyIndex);
		}
		
		return true;
	}
	
	
	private boolean stealFromLeft(InnerNode parent, int nodeKeyIndex) {
		if (nodeKeyIndex == 0) {
			return false;
		}
		
		Node[] children = parent.getChildren();
		Node right = children[nodeKeyIndex];
		Node left = children[nodeKeyIndex - 1];
		
		// check whether left node is eligible for stealing from it
		if (countKeys(left.getKeys()) <= capacity / 2)
			return false;
		
		if (left instanceof LeafNode)
			leftLeafStealing((LeafNode)left, (LeafNode)right, parent, nodeKeyIndex - 1);
		else
			leftInnerNodeStealing((InnerNode) left, (InnerNode) right, parent, nodeKeyIndex - 1);
		
		return true;
	}
	
	
	private void rightLeafStealing(LeafNode left, LeafNode right, InnerNode parent, int splitKeyIndex) {
		doSteal(left, right, parent, splitKeyIndex, true);
	}
	
	
	private void leftLeafStealing(LeafNode left, LeafNode right, InnerNode parent, int splitKeyIndex) {
		doSteal(left, right, parent, splitKeyIndex, false);
	}
	
	
	private void doSteal(LeafNode left, LeafNode right, InnerNode parent, int splitKeyIndex, boolean includeMedian) {
		
		int leftKeyCount = countKeys(left.getKeys());
		int rightKeyCount = countKeys(right.getKeys());
		
		// collect keys
		Integer[] keyBuffer = ArrayUtils.EMPTY_INTEGER_OBJECT_ARRAY;
		
		keyBuffer = addArrayNonNullValues(keyBuffer, left.getKeys(), leftKeyCount, Integer.class);
		keyBuffer = addArrayNonNullValues(keyBuffer, right.getKeys(), rightKeyCount, Integer.class);
		
		// collect values
		String[] valueBuffer = ArrayUtils.EMPTY_STRING_ARRAY;
		valueBuffer = addArrayNonNullValues(valueBuffer, left.getValues(), leftKeyCount, String.class);
		valueBuffer = addArrayNonNullValues(valueBuffer, right.getValues(), rightKeyCount, String.class);
		
		// find median
		int medianIndex = keyBuffer.length / 2;
		Integer medianKey = keyBuffer[medianIndex];
		
		// When two leafs are merged, there usually remains a bigger and a smaller leaf. Depending whether the median
		// key gets added to the left or the right leaf, one can chain multiple merges in left and right direction.
		//
		// If consecutively merging several leafs to the right, it is important that the merged left leafs remain valid.
		// Tree validation condition (key count >= capacity /2) must hold.
		if (includeMedian && keyBuffer.length < capacity) {
			// Shifting the median is actually stealing from right siblings. Left siblings are already sorted by the
			// normal behavior.
			medianIndex += 1;
		}
		
		// distribute the keys and values among both leafs
		left.setKeys(ArrayUtils.subarray(keyBuffer, 0, medianIndex));
		left.setValues(ArrayUtils.subarray(valueBuffer, 0, medianIndex));
		right.setKeys(ArrayUtils.subarray(keyBuffer, medianIndex, keyBuffer.length));
		right.setValues(ArrayUtils.subarray(valueBuffer, medianIndex, keyBuffer.length));
		
		// set new key in parent
		parent.getKeys()[splitKeyIndex] = medianKey;
	}
	
	
	private void rightInnerNodeStealing(InnerNode left, InnerNode right, InnerNode parent, int splitKeyIndex) {
		doStealInnerNodeRight(left, right, parent, splitKeyIndex, true);
	}
	
	
	private void leftInnerNodeStealing(InnerNode left, InnerNode right, InnerNode parent, int splitKeyIndex) {
		doStealInnerNodeLeft(left, right, parent, splitKeyIndex, false);
	}
	
	
	private void doStealInnerNodeLeft(InnerNode left, InnerNode right, InnerNode parent, int splitKeyIndex, boolean includeMedian) {
		
		int leftKeyCount = countKeys(left.getKeys());
		int rightKeyCount = countKeys(right.getKeys());
		
		Integer newKeyInParent = left.getKeys()[leftKeyCount - 1];
		Node stolenChild = left.getChildren()[leftKeyCount];

		// remove last key and child from left node
		left.getKeys()[leftKeyCount - 1] = null;
		left.getChildren()[leftKeyCount] = null;
		
		// collect keys for right node
		Integer[] keyBuffer = ArrayUtils.EMPTY_INTEGER_OBJECT_ARRAY;
		Integer newKeyRightChild = parent.getKeys()[splitKeyIndex];

		keyBuffer = ArrayUtils.add(keyBuffer, newKeyRightChild);
		keyBuffer = addArrayNonNullValues(keyBuffer, right.getKeys(), rightKeyCount, Integer.class);
		
		// collect nodes
		Node[] valueBuffer = new Node[0];
		valueBuffer = ArrayUtils.add(valueBuffer, stolenChild);
		valueBuffer = addArrayNonNullValues(valueBuffer, right.getChildren(), rightKeyCount + 1, Node.class);
		
		// set keys and children for right node
		right.setKeys(ArrayUtils.subarray(keyBuffer, 0, valueBuffer.length));
		right.setChildren(ArrayUtils.subarray(valueBuffer, 0, valueBuffer.length));
		
		// set new key in parent
		parent.getKeys()[splitKeyIndex] = newKeyInParent;
	}
	
	
	private void doStealInnerNodeRight(InnerNode left, InnerNode right, InnerNode parent, int splitKeyIndex, boolean includeMedian) {
		
		int leftKeyCount = countKeys(left.getKeys());
		int rightKeyCount = countKeys(right.getKeys());
		
		Integer newKeyInParent = right.getKeys()[0];
		Node stolenChild = right.getChildren()[0];
		
		// collect keys for right node
		Integer[] keyBuffer = ArrayUtils.EMPTY_INTEGER_OBJECT_ARRAY;
		Integer newKeyLeftChild = stolenChild.getKeys()[0];
		left.getKeys()[leftKeyCount] = newKeyLeftChild;
		left.getChildren()[leftKeyCount+1] = stolenChild;
		keyBuffer = addArrayNonNullValues(keyBuffer, ArrayUtils.subarray(right.getKeys(), 1, rightKeyCount), null, Integer.class);
		
		// collect nodes
		Node[] valueBuffer = new Node[0];
		valueBuffer = addArrayNonNullValues(valueBuffer, ArrayUtils.subarray(right.getChildren(), 1 ,rightKeyCount + 1), null, Node.class);
		
		// set keys and children for right node
		right.setKeys(ArrayUtils.subarray(keyBuffer, 0, valueBuffer.length));
		right.setChildren(ArrayUtils.subarray(valueBuffer, 0, valueBuffer.length));
		
		// set new key in parent
		parent.getKeys()[splitKeyIndex] = newKeyInParent;
	}
	
	
	@SuppressWarnings("unchecked")
	private static <T> T[] addArrayNonNullValues(T[] buffer, T[] values, Integer endExclusive, Class<T> cls) {
		return ArrayUtils.addAll(buffer, Arrays.stream(values, 0, endExclusive == null ? values.length : endExclusive)
				.filter(Objects::nonNull)
				.toArray((size) -> (T[]) Array.newInstance(cls, size)));
	}
	
	
	private boolean isValid(Node node) {
		int keysCount = countKeys(node.getKeys());
		return keysCount >= capacity / 2 || (rootNode().equals(node) && keysCount > 0);
	}
	
	
	
	
	private void mergeInnerNodes(Integer key, Deque<InnerNode> parents) {
		
		InnerNode parent = parents.pop();
		int parentChildIndex = getParentChildIndex(key, parent);
		
		Integer[] keys = parent.getKeys();
		int keysCount = countKeys(keys);
		Node[] children = parent.getChildren();
		Node node = children[parentChildIndex];
		
		Integer[] keyBuffer = ArrayUtils.EMPTY_INTEGER_OBJECT_ARRAY;
		Node[] nodesBuffer = new Node[0];
		
		// check if merge with right node is possible
		if (parentChildIndex < keysCount && parentChildIndex != -1) {
			
			keyBuffer = addArrayNonNullValues(keyBuffer, node.getKeys(), null, Integer.class);
			nodesBuffer = addArrayNonNullValues(nodesBuffer, (Node[]) node.getPayload(), null, Node.class);
			
			keyBuffer = ArrayUtils.add(keyBuffer, keys[parentChildIndex]);
			
			Node right = children[parentChildIndex + 1];
			keyBuffer = addArrayNonNullValues(keyBuffer, right.getKeys(), null, Integer.class);
			nodesBuffer = addArrayNonNullValues(nodesBuffer, (Node[]) right.getPayload(), null, Node.class);
			
			InnerNode child = BPlusTreeUtilities.newNode(
					keys(ArrayUtils.subarray(keyBuffer, 0, keyBuffer.length)),
					nodes(ArrayUtils.subarray(nodesBuffer, 0, nodesBuffer.length)),
					capacity);
			
			children[parentChildIndex] = child;
			int i = parentChildIndex + 1;
			children[i] = null;
			while (i < capacity && children[i + 1] != null) {
				children[i] = children[i + 1];
				children[i + 1] = null;
				i++;
			}
			i = parentChildIndex;
			keys[i] = null;
			while (i < capacity - 1 && keys[i + 1] != null) {
				keys[i] = keys[i + 1];
				keys[i + 1] = null;
				i++;
			}
			
		}
		
		// if not possible merge with left node
		else {
			Node left = children[parentChildIndex -1];
			keyBuffer = addArrayNonNullValues(keyBuffer, left.getKeys(), null, Integer.class);
			nodesBuffer = addArrayNonNullValues(nodesBuffer, (Node[]) left.getPayload(), null, Node.class);
			
			keyBuffer = ArrayUtils.add(keyBuffer, keys[parentChildIndex -1]);
			
			keyBuffer = addArrayNonNullValues(keyBuffer, node.getKeys(), null, Integer.class);
			nodesBuffer = addArrayNonNullValues(nodesBuffer, (Node[]) node.getPayload(), null, Node.class);
			
			
			InnerNode child = BPlusTreeUtilities.newNode(
					keys(ArrayUtils.subarray(keyBuffer, 0, keyBuffer.length)),
					nodes(ArrayUtils.subarray(nodesBuffer, 0, nodesBuffer.length)),
					capacity);
			
			children[parentChildIndex -1 ] = child;
			int i = parentChildIndex;
			children[i] = null;
			keys[i -1] = null;
			while (i < capacity && children[i + 1] != null) {
				children[i] = children[i + 1];
				i++;
			}
			i = parentChildIndex;
			keys[i] = null;
			while (i < capacity - 1 && keys[i + 1] != null) {
				keys[i] = keys[i + 1];
				i++;
			}
		}
		
		if (!isValid(parent)) {
			
			// this must work! Period.
			// if parent is invalid, it only has a single key and two children. All keys and all values are already
			// contained in both of the buffers. Thus, creating a new leaf from those buffers and setting it as the root
			// suffices.
			if (parent.equals(rootNode())) {
				root = BPlusTreeUtilities.newNode(
						createArray(keyBuffer, capacity),
						createArray(nodesBuffer, capacity + 1),
						capacity);
				return;
			}
			stealOrMerge(key, parents);
//			reorderInnerNodes(parents);
		}
	}
	
	public static <T> T[] insert(int index, T[] array, T... values) {
		if (array == null) {
			return null;
		} else if (values != null && values.length != 0) {
			if (index >= 0 && index <= array.length) {
				Class<?> type = array.getClass().getComponentType();
				T[] result = (T[])((Object[]) Array.newInstance(type, array.length + values.length));
				System.arraycopy(values, 0, result, index, values.length);
				if (index > 0) {
					System.arraycopy(array, 0, result, 0, index);
				}
				
				if (index < array.length) {
					System.arraycopy(array, index, result, index + values.length, array.length - index);
				}
				
				return result;
			} else {
				throw new IndexOutOfBoundsException("Index: " + index + ", Length: " + array.length);
			}
		} else {
			return ArrayUtils.clone(array);
		}
	}
	
}
